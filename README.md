## Aplicacion prueba gradle

https://schuchert.wikispaces.com/gradle.GettingStarted

https://spring.io/guides/gs/gradle/



gradle.GettingStarted
Background
These are my preliminary notes on getting started with gradle. The last time I did any serious build work, I used ant, before that make and nmake; I missed using maven so coming into gradle was both easy and frustrating. Even though I did not use maven extensively, I knew its model well enough to get up and running somewhat quickly.

Gradle requires a JVM; I work mostly on OS X, so I use JSE 1.6.0_24, but any version from 1.5 on should work fine. What follows assumes you already have a JVM running. I start in a shell and only at the end finally get into an IDE.

Install Gradle
Instructions for installing Gradle are here. I'm summarizing them here, if you have any questions refer to the installation instructions.

    Download Gradle. For these examples I happen to be using the 1.0-milestone-3 version of Gradle.
    Unzip the file somewhere, say /users/Schuchert/bin
    This will create a directory under the installation directory called something like gradle-1.0-milestone-3/
    Add the bin directory under the extracted directory to your path. In my particular case, the path is: /Users/schuchert/bin/gradle-1.0-milestone-3/bin/
    Verify your installation works by simply typing gradle:

[~]% gradle
:help
 
Welcome to Gradle 1.0-milestone-3.
 
To run a build, run gradle <task> ...
 
To see a list of available tasks, run gradle tasks
 
To see a list of command-line options, run gradle --help
 
BUILD SUCCESSFUL
 
Total time: 4.304 secs
[~]%


Create Initial Project Structure
Gradle prefers convention over configuration and its default assumptions about project structure mirror those of Maven. For this example, let's assume that you'll create a single project directory (this is to keep things simple). Also, this example demonstrates working with Java, though Gradle supports several other languages.

    Create a project directory, e.g. ~/src/gradle_example
    Under that directory create src, src/main/, src/main/java, src/test, src/test/java

[~]% mkdir -p ~/src/gradle_example
[~]% cd ~/src/gradle_example
/Users/schuchert/src/gradle_example
[~/src/gradle_example]% mkdir -p src/main/java src/main/resources src/test/java


Create Basic Gradle Build File
This example uses java and will ultimately use Eclipse, so here's a basic build file that will support building and running tests if your project follows Maven layout conventions, uses JUnit and uses Maven Central to retrieve jar files:

apply plugin: 'java'
 
repositories {
  mavenCentral()
}
 
dependencies {
  testCompile group: 'junit', name: 'junit', version: '4.8+'
}

    Create a file called biuld.gradle under your project directory (~/src/gradle_example in my case)
    Verify that a build works (even without any production or test code)

[~/src/gradle_example]% gradle test
:compileJava UP-TO-DATE
:processResources UP-TO-DATE
:classes UP-TO-DATE
:compileTestJava UP-TO-DATE
:processTestResources UP-TO-DATE
:testClasses UP-TO-DATE
:test
 
BUILD SUCCESSFUL
 
Total time: 9.219 secs
[~/src/gradle_example]%


Smoke Test With A Little Code
We're working at the command line. Java packages relate to directories. Now you'll create a couple of source files, one test, one production. I'll use the package demo.rpn:

[~/src/gradle_example]% mkdir -p src/test/java/demo/rpn src/main/java/demo/rpn


    Under the project directory, in src/test/java/demo/rpn create the file RpnCalculatorShould.java:

[~/src/gradle_example]% vi src/test/java/demo/rpn/RpnCalculatorShould.java


RpnCalculatorShould.java

package demo.rpn;
 
import static org.junit.Assert.assertEquals;
 
import org.junit.Test;
 
public class RpnCalculatorShould {
    @Test
    public void addTwoNumbersCorreclty() {
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.digit(4);
        rpnCalculator.enter();
        rpnCalculator.digit(9);
        rpnCalculator.operator("+");
        assertEquals(13, rpnCalculator.xRegister());
    }
}


    Under the project directory, in src.main/java/demo/rpn create the file RpnCalculator.java:

[~/src/gradle_example]% vi src/main/java/demo/rpn/RpnCalculator.java


RpnCalculator.java

package demo.rpn;
 
public class RpnCalculator {
    public void digit(int value) {
    }
 
    public void enter() {
    }
 
    public void operator(String operatorName) {
    }
 
    public int xRegister() {
        return 13;
    }
}


    Run your tests by typing gradle test

[~/src/gradle_example]% gradle test
:compileJava
:processResources UP-TO-DATE
:classes
:compileTestJava
:processTestResources UP-TO-DATE
:testClasses
:test
 
BUILD SUCCESSFUL
 
Total time: 9.824 secs
[~/src/gradle_example]%


Here there is a bit of a visual affordance problem. Gradle will show test execution while they are running. Once the test run, and if they all pass, the output just shows that the test task executed. However, Gradle produced some output. You can verify that your one tests executed successfully by looking at the build directory:

open build/reports/tests/index.html


My report shows one test running and passing.
On To Eclipse
If you want to create project information for Eclipse, you need to do a few things:

    Add apply plugin: 'eclipse'
    Create the project structure using gradle eclipse

[~/src/gradle_example]% gradle eclipse
:eclipseClasspath
:eclipseJdt
:eclipseProject
:eclipse
 
BUILD SUCCESSFUL
 
Total time: 8.383 secs
[~/src/gradle_example]%


Note, this creates a project directory, not a workspace. You'll need to:

    Start eclipse
    Select a workspace
    Import the project into eclipse


In my example, I only created a top-level project directory, not a workspace directory. However, you can create an Eclipse workspace and include a symbolic link to the project. This is a bit out of scope for what I want to get covered. I only mention this here because it confused me a bit.
Preparing for git
No coding is complete without a mention of using a repository. We'll do a touch of preparation for git and check in our initial structure. In the top-level project directory create a file called .gitignore. For this example, and assuming you have executed the Eclipse step above:

build
.classpath
.project
.gradle
.settings


You might choose to not include the .settings directory used by Eclipse.

Now you can turn this directory into a git repository:

    Initialize git using git init
    Add the files you care to monitor using git add .gitignore src build.gradle
    Verify everything is cool using git status
    Finally, commit the changes (adding just staged the work), using git commit -m "Initial Commit"


[~/src/gradle_example]% git init
Initialized empty Git repository in /Users/schuchert/src/gradle_example/.git/
[~/src/gradle_example]% git add .gitignore src build.gradle
[~/src/gradle_example]% git status
# On branch master
#
# Initial commit
#
# Changes to be committed:
#   (use "git rm --cached <file>..." to unstage)
#
#    new file:   .gitignore
#    new file:   build.gradle
#    new file:   src/main/java/demo/rpn/RpnCalculator.java
#    new file:   src/test/java/demo/rpn/RpnCalculatorShould.java
#
[~/src/gradle_example]% git commit -m "Initial commit"
[master (root-commit) 1efa8fd] Initial commit
 4 files changed, 52 insertions(+), 0 deletions(-)
 create mode 100644 .gitignore
 create mode 100644 build.gradle
 create mode 100644 src/main/java/demo/rpn/RpnCalculator.java
 create mode 100644 src/test/java/demo/rpn/RpnCalculatorShould.java
[~/src/gradle_example]%


Congratulations
The purpose of this was to give one specific example of using Gradle to build a new project from scratch. Here's a summary of the steps:

    Create a project directory
    Create an initial build.gradle file
    Add some test files and some production code files using maven project layout
    Build and run your tests
    Optionally create the Eclipse project structure
    Put your work into git


Hope this helps get you started a touch quicker. For more information on Gradle, have a look at its extensive documentation.

Final Caution
Since we are using Maven repositories, that comes with the standard baggage associated with it. For example, if you are using a project that uses a particular release of another project, you might need to configure a few more things in the build.gradle file to make it possible to find everything. Here is an example file from another project I've been toying with:

apply plugin: 'java'
apply plugin: 'eclipse'
 
repositories {
    mavenCentral()
    mavenRepo name: 'hibernate',
        urls: 'https://repository.jboss.org/nexus/content/groups/public/'
    mavenRepo name: 'ERB Spring Release Repo',
            urls: 'http://repository.springsource.com/maven/bundles/release'
    mavenRepo name: 'ERB External Spring Release Repo',
            urls: 'http://repository.springsource.com/maven/bundles/external'
}
 
dependencies {
    testCompile group: 'org.dspace.dependencies.jmockit',
        name: 'dspace-jmockit',
        version: '0.999.4'
    testCompile group: 'junit', name: 'junit', version: '4.8+'
 
    runtime group: 'org.apache.derby', name: 'derby', version: '10.8+'
 
    compile group: 'org.hibernate', name: 'hibernate', version: '3+'
    compile group: 'org.hibernate', name: 'hibernate-annotations', version: '3+'
    compile group: 'org.hibernate', name: 'hibernate-entitymanager', version: '3+'
    compile group: 'org.hibernate', name: 'hibernate-tools', version: '3+'
    runtime group: 'org.hibernate.java-persistence',
                    name:  'jpa-api', version: '2+'
 
    compile group: 'org.synyx.hades',
                    name:  'org.synyx.hades', version: '2.+'
 
  runtime group: 'org.slf4j', name: 'slf4j-api', version: '1.6.1'
}


In this case I'm using Hibernate, which uses Spring. however, the spring release it uses is from another maven repository, so I've included a few named maven repositories as well.

Good luck and I recommend giving Gradle a try. While this example barely scratches the surface, Gradle uses scripting and convention over configuration, so it appears to result in easier to read/write/follow build files. 



-------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
###########################################################################################################################################
###########################################################################################################################################
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------



Getting Started
Building Java Projects with Gradle

This guide walks you through using Gradle to build a simple Java project.
What you’ll build

You’ll create a simple app and then build it using Gradle.
What you’ll need

    About 15 minutes

    A favorite text editor or IDE

    JDK 6 or later

How to complete this guide

Like most Spring Getting Started guides, you can start from scratch and complete each step, or you can bypass basic setup steps that are already familiar to you. Either way, you end up with working code.

To start from scratch, move on to Set up the project.

To skip the basics, do the following:

    Download and unzip the source repository for this guide, or clone it using Git: git clone https://github.com/spring-guides/gs-gradle.git

    cd into gs-gradle/initial

    Jump ahead to Install Gradle.

When you’re finished, you can check your results against the code in gs-gradle/complete.
Set up the project

First you set up a Java project for Gradle to build. To keep the focus on Gradle, make the project as simple as possible for now.
Create the directory structure

In a project directory of your choosing, create the following subdirectory structure; for example, with mkdir -p src/main/java/hello on *nix systems:

└── src
    └── main
        └── java
            └── hello

Within the src/main/java/hello directory, you can create any Java classes you want. For simplicity’s sake and for consistency with the rest of this guide, Spring recommends that you create two classes: HelloWorld.java and Greeter.java.

src/main/java/hello/HelloWorld.java

package hello;

public class HelloWorld {
  public static void main(String[] args) {
    Greeter greeter = new Greeter();
    System.out.println(greeter.sayHello());
  }
}

src/main/java/hello/Greeter.java

package hello;

public class Greeter {
  public String sayHello() {
    return "Hello world!";
  }
}

Install Gradle

Now that you have a project that you can build with Gradle, you can install Gradle.

Gradle is downloadable as a zip file at http://www.gradle.org/downloads. Only the binaries are required, so look for the link to gradle-version-bin.zip. (You can also choose gradle-version-all.zip to get the sources and documentation as well as the binaries.)

Unzip the file to your computer, and add the bin folder to your path.

To test the Gradle installation, run Gradle from the command-line:

gradle

If all goes well, you see a welcome message:

:help

Welcome to Gradle 1.8.

To run a build, run gradle <task> ...

To see a list of available tasks, run gradle tasks

To see a list of command-line options, run gradle --help

BUILD SUCCESSFUL

Total time: 2.675 secs

You now have Gradle installed.
Find out what Gradle can do

Now that Gradle is installed, see what it can do. Before you even create a build.gradle file for the project, you can ask it what tasks are available:

gradle tasks

You should see a list of available tasks. Assuming you run Gradle in a folder that doesn’t already have a build.gradle file, you’ll see some very elementary tasks such as this:

:tasks

== All tasks runnable from root project

== Build Setup tasks
setupBuild - Initializes a new Gradle build. [incubating]
wrapper - Generates Gradle wrapper files. [incubating]

== Help tasks
dependencies - Displays all dependencies declared in root project 'gs-gradle'.
dependencyInsight - Displays the insight into a specific dependency in root project 'gs-gradle'.
help - Displays a help message
projects - Displays the sub-projects of root project 'gs-gradle'.
properties - Displays the properties of root project 'gs-gradle'.
tasks - Displays the tasks runnable from root project 'gs-gradle'.

To see all tasks and more detail, run with --all.

BUILD SUCCESSFUL

Total time: 3.077 secs

Even though these tasks are available, they don’t offer much value without a project build configuration. As you flesh out the build.gradle file, some tasks will be more useful. The list of tasks will grow as you add plugins to build.gradle, so you’ll occasionally want to run tasks again to see what tasks are available.

Speaking of adding plugins, next you add a plugin that enables basic Java build functionality.
Build Java code

Starting simple, create a very basic build.gradle file that has only one line in it:

apply plugin: 'java'

This single line in the build configuration brings a significant amount of power. Run gradle tasks again, and you see new tasks added to the list, including tasks for building the project, creating JavaDoc, and running tests.

You’ll use the gradle build task frequently. This task compiles, tests, and assembles the code into a JAR file. You can run it like this:

gradle build

After a few seconds, "BUILD SUCCESSFUL" indicates that the build has completed.

To see the results of the build effort, take a look in the build folder. Therein you’ll find several directories, including these three notable folders:

    classes. The project’s compiled .class files.

    reports. Reports produced by the build (such as test reports).

    libs. Assembled project libraries (usually JAR and/or WAR files).

The classes folder has .class files that are generated from compiling the Java code. Specifically, you should find HelloWorld.class and Greeter.class.

At this point, the project doesn’t have any library dependencies, so there’s nothing in the dependency_cache folder.

The reports folder should contain a report of running unit tests on the project. Because the project doesn’t yet have any unit tests, that report will be uninteresting.

The libs folder should contain a JAR file that is named after the project’s folder. Further down, you’ll see how you can specify the name of the JAR and its version.
Declare dependencies

The simple Hello World sample is completely self-contained and does not depend on any additional libraries. Most applications, however, depend on external libraries to handle common and/or complex functionality.

For example, suppose that in addition to saying "Hello World!", you want the application to print the current date and time. You could use the date and time facilities in the native Java libraries, but you can make things more interesting by using the Joda Time libraries.

First, change HelloWorld.java to look like this:

package hello;

import org.joda.time.LocalTime;

public class HelloWorld {
  public static void main(String[] args) {
    LocalTime currentTime = new LocalTime();
    System.out.println("The current local time is: " + currentTime);

    Greeter greeter = new Greeter();
    System.out.println(greeter.sayHello());
  }
}

Here HelloWorld uses Joda Time’s LocalTime class to get and print the current time.

If you ran gradle build to build the project now, the build would fail because you have not declared Joda Time as a compile dependency in the build.

For starters, you need to add a source for 3rd party libraries.

repositories {
    mavenLocal()
    mavenCentral()
}

The repositories block indicates that the build should resolve its dependencies from the Maven Central repository. Gradle leans heavily on many conventions and facilities established by the Maven build tool, including the option of using Maven Central as a source of library dependencies.

Now that we’re ready for 3rd party libraries, let’s declare some.

dependencies {
    compile "joda-time:joda-time:2.2"
}

With the dependencies block, you declare a single dependency for Joda Time. Specifically, you’re asking for (reading right to left) version 2.2 of the joda-time library, in the joda-time group.

Another thing to note about this dependency is that it is a compile dependency, indicating that it should be available during compile-time (and if you were building a WAR file, included in the /WEB-INF/libs folder of the WAR). Other notable types of dependencies include:

    providedCompile. Required dependencies for compiling the project code, but that will be provided at runtime by a container running the code (for example, the Java Servlet API).

    testCompile. Dependencies used for compiling and running tests, but not required for building or running the project’s runtime code.

Finally, let’s specify the name for our JAR artifact.

jar {
    baseName = 'gs-gradle'
    version =  '0.1.0'
}

The jar block specifies how the JAR file will be named. In this case, it will render gs-gradle-0.1.0.jar.

Now if you run gradle build, Gradle should resolve the Joda Time dependency from the Maven Central repository and the build will succeed.
Build your project with Gradle Wrapper

The Gradle Wrapper is the preferred way of starting a Gradle build. It consists of a batch script for Windows and a shell script for OS X and Linux. These scripts allow you to run a Gradle build without requiring that Gradle be installed on your system. To make this possible, add the following block to the bottom of your build.gradle.

task wrapper(type: Wrapper) {
    gradleVersion = '1.11'
}

Run the following command to download and initialize the wrapper scripts:

gradle wrapper

After this task completes, you will notice a few new files. The two scripts are in the root of the folder, while the wrapper jar and properties files have been added to a new gradle/wrapper folder.

└── initial
    └── gradlew
    └── gradlew.bat
    └── gradle
        └── wrapper
            └── gradle-wrapper.jar
            └── gradle-wrapper.properties

The Gradle Wrapper is now available for building your project. Add it to your version control system, and everyone that clones your project can build it just the same. It can be used in the exact same way as an installed version of Gradle. Run the wrapper script to perform the build task, just like you did previously:

./gradlew build

The first time you run the wrapper for a specified version of Gradle, it downloads and caches the Gradle binaries for that version. The Gradle Wrapper files are designed to be committed to source control so that anyone can build the project without having to first install and configure a specific version of Gradle.

At this stage, you will have built your code. You can see the results here:

build
├── classes
│   └── main
│       └── hello
│           ├── Greeter.class
│           └── HelloWorld.class
├── dependency-cache
├── libs
│   └── gs-gradle-0.1.0.jar
└── tmp
    └── jar
        └── MANIFEST.MF

Included are the two expected class files for Greeter and HelloWorld, as well as a JAR file. Take a quick peek:

$ jar tvf build/libs/gs-gradle-0.1.0.jar
  0 Fri May 30 16:02:32 CDT 2014 META-INF/
 25 Fri May 30 16:02:32 CDT 2014 META-INF/MANIFEST.MF
  0 Fri May 30 16:02:32 CDT 2014 hello/
369 Fri May 30 16:02:32 CDT 2014 hello/Greeter.class
988 Fri May 30 16:02:32 CDT 2014 hello/HelloWorld.class

The class files are bundled up. It’s important to note, that even though you declared joda-time as a dependency, the library isn’t included here. And the JAR file isn’t runnable either.

To make this code runnable, we can use gradle’s application plugin. Add this to your build.gradle file.

apply plugin: 'application'

mainClassName = 'hello.HelloWorld'

Then you can run the app!

$ ./gradlew run
:compileJava UP-TO-DATE
:processResources UP-TO-DATE
:classes UP-TO-DATE
:run
The current local time is: 16:16:20.544
Hello world!

BUILD SUCCESSFUL

Total time: 3.798 secs

To bundle up dependencies requires more thought. For example, if we were building a WAR file, a format commonly associated with packing in 3rd party dependencies, we could use gradle’s WAR plugin. If you are using Spring Boot and want a runnable JAR file, the spring-boot-gradle-plugin is quite handy. At this stage, gradle doesn’t know enough about your system to make a choice. But for now, this should be enough to get started using gradle.

To wrap things up for this guide, here is the completed build.gradle file:

build.gradle

apply plugin: 'java'
apply plugin: 'eclipse'
apply plugin: 'application'

mainClassName = 'hello.HelloWorld'

// tag::repositories[]
repositories {
    mavenLocal()
    mavenCentral()
}
// end::repositories[]

// tag::jar[]
jar {
    baseName = 'gs-gradle'
    version =  '0.1.0'
}
// end::jar[]

// tag::dependencies[]
dependencies {
    compile "joda-time:joda-time:2.2"
}
// end::dependencies[]

// tag::wrapper[]
task wrapper(type: Wrapper) {
    gradleVersion = '1.11'
}
// end::wrapper[]

    There are many start/end comments embedded here. This makes it possible to extract bits of the build file into this guide for the detailed explanations above. You don’t need them in your production build file.
Summary

Congratulations! You have now created a simple yet effective Gradle build file for building Java projects.




